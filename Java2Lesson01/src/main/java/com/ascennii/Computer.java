package com.ascennii;

public class Computer {
    private int ssd = 500;
    private long ram = 1024;

    Computer() {
        System.out.println("Computer was created");
    }

    Computer(int ssd) {
        this.ssd = ssd;
    }

    public Computer(int ssd, long ram) {
        this.ssd = ssd;
        this.ram = ram;
    }

    void load() {
        System.out.println("Computer started");
    }

    public void state() {
        System.out.println("SSD: " + ssd);
        System.out.println("HDD: " + ram);
    }
}
