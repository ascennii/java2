package com.ascennii;

public class ComputerRunner {
    void start(){
        Computer comp = new Computer(500, 1000);
        comp.load();
        comp.state();
        Computer c = new Computer();
    }
}
