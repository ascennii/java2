package com.ascennii;

import java.util.Arrays;

/**
 * 5. Написать стороку разбивающую строку на равные части по N символов
 * и сохраняющую их в массив.
 */
public class Task5 {
    public static void main(String[] args) {
        String input = "Однажды в студеную зимнюю пору, я из лесу вышел был сильный мороз";
        int n = 3;
        String[] result = splitString(input, n);
        System.out.println(Arrays.toString(result));
    }

    public static String[] splitString(String input, int length){

        int arraySize = (int)Math.ceil(input.length() / (double)length);
        System.out.println(arraySize);

        String[] result = new String[arraySize];
        int counter = 0;
        for (int i = 0; i < input.length() ; i += length) {
            result[counter] = i+length < input.length() ? input.substring(i, i+length) : input.substring(i) ;
            System.out.println(result[counter]);
            counter++;
        }

        return result;
    }
}
