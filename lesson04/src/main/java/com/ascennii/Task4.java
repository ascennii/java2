package com.ascennii;

/**
 * 4. Посчитать количество всех точек запятых и восклицательных знаков.
 */
public class Task4 {
    public static void main(String[] args) {
        String input = "Привет. А мы не виделись, с тобой, сто лет!";
        int count = countOf(input);
        System.out.println(count);
    }

    public static int countOf(String input){
        String output = input.replace(".","")
                .replace("!","")
                .replace(",","");
        return input.length() - output.length();
    }
}
