package com.ascennii;

public class Main {
    public static void main(String[] args) {

        String message = "Привет как дела :( нормально? :( или так себе :)";
        System.out.println(replaceEmogy(message));
    }

    /**
     * Заменить грустные смайлики на веселые
     */
    public static String replaceEmogy(String message){
        String newMessage = message.replace(":(", ":)" );
        return newMessage;
    }

}