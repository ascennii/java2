package com.ascennii;

/**
 * 2. Написать функцию котрая принимает строку и слово
 * и возвращает true если строка начинается или заканчивается этим словом.
 */
public class Task2 {
    public static void main(String[] args){
        String line = "Снег на окнах рисует снег";
        String word = "снег";

        boolean result = checkFirstAndLastWord(line, word);
        System.out.println(result);
    }

    public static boolean checkFirstAndLastWord(String line, String word){
        return line.toLowerCase().startsWith(word.toLowerCase()) && line.endsWith(word.toLowerCase());
    }
}
