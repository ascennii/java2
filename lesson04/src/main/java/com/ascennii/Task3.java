package com.ascennii;

/**
 * Принимае фамилию имя отчество, возвращет Ф.И.О
 */
public class Task3 {
    public static void main(String[] args) {
        String surname = "Znam";
        String name = "Alex";
        String patronymic = "Andreevich";

        System.out.println(fio(surname, name, patronymic));
    }

    public static String fio(String surname, String name, String patronymic) {

        String result = Character.toUpperCase(surname.charAt(0)) + "."
                + name.toUpperCase().charAt(0) + "."
                + patronymic.toUpperCase().charAt(0) + ".";

        return result;
    }
}
