package com.ascennii.l05014;

public class EnumRunner {
    public static void main(String[] args) {
        ProcessorType pt = ProcessorType.BIT_32;
        System.out.println(pt);
        System.out.println(pt.valueOf("BIT_32"));
        System.out.println(pt.BIT_32.ordinal());
        System.out.println(ProcessorType.BIT_32.getName());
        System.out.println(ProcessorType.BIT_32.getDescription());
        System.out.println(pt.getWeight()+ 10);
    }
}
