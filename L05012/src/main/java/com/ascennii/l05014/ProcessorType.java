package com.ascennii.l05014;

public enum ProcessorType implements Describble {
    BIT_32("bit-32"){
        @Override
        public String getDescription() {
            return "Описаание первого";
        }
        @Override
        public int getWeight() {
            return 10;
        }
    },
    BIT_64("bit-64"){
        @Override
        public String getDescription() {
            return "Описание второго";
        }

        @Override
        public int getWeight() {
            return 15;
        }
    };

    String name;

    ProcessorType(String name) {
        this.name = name;
    }

    public abstract String getDescription();

    public String getName() {
        return name;
    }
}
