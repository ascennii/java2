package com.ascennii.game;

public class Archer extends Hero{
    public int damage = 10;
    private Wolf wolf;
    public Archer(String name, int damage) {
        super(name, damage);
        this.wolf = new Wolf("Wolf", 4);
    }

    @Override
    public void attackEnemy(Enemy enemy){
        System.out.println(getName() + " атакует врага!");
        enemy.takeDamage(damage + Archer.this.damage);
    }

    class Wolf{
        private String name;
        private int damange;

        public Wolf(String name, int damange) {
            this.name = name;
            this.damange = damange;
        }
    }
}
