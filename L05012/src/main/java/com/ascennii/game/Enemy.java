package com.ascennii.game;

public class Enemy implements Mortal {
    private int health;
    private String name;
    public Enemy(String name, int health){
        this.health = health;
        this.name = name;
    }

    public boolean isAlive(){
        return health > 0;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health){
        this.health = health;
    }

    public void takeDamage(int damage){
        health -= Math.min(health, damage);
        System.out.println("Получен урон:" + damage + "\nОсталось " + health + " ед. здоровья.");
    }
}
