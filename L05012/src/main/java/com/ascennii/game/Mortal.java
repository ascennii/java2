package com.ascennii.game;

public interface Mortal {
    boolean isAlive();
}
