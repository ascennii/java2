package com.ascennii;

public class LessonRunner {
    public static void main(String[] args) {
        Computer laptop = new Laptop(new Ssd(1024), new Ram(512));
        Computer mobile = new Mobile(new Ssd(256), new Ram(256));
        loadComputers(laptop, mobile);
        printInfo(laptop, mobile);
    }
    
    public static void loadComputers(Computer... computers){
        for (Computer computer: computers) {
            computer.load();
            computer.print();
        }
    }

    public static void printInfo(Computer... computers){
        for (Computer computer: computers){
            computer.print();
            if (computer instanceof Laptop){
                ((Laptop)computer).open();
            }
        }
    }
}
