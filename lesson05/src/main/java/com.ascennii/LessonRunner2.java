package com.ascennii;

public class LessonRunner2 {
    public static void main(String[] args) {
        Computer laptop = new Laptop(new Ssd(1024), new Ram(512));
        Computer mobile = new Mobile(new Ssd(256), new Ram(256));

        print(laptop, mobile);
    }

    public static void print(Printable... objects){
        for(Printable o: objects){
            o.print();
        }
    }
}
