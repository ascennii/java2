package com.ascennii;

public class Mobile extends Computer{

    public Mobile(Ssd ssd, Ram ram) {
        super(ssd, ram);
    }

    @Override
    public void load() {
        System.out.println("Mobile is load");
    }

    public void print() {
        System.out.println("Ssd: " + ssd.getValue() + "\n" + "Ram: " + ram.getValue());
    }
}
