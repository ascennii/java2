package com.ascennii;

import java.util.Random;

public interface Printable {
    String SOME_VALUE = "Example";
    Random random = new Random();
    void print();

    default void printRandom(){
        Random random = new Random();
        System.out.println(random.nextInt());
    }
}
