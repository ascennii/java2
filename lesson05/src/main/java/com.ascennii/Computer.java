package com.ascennii;

public abstract class Computer implements Printable {
    public Ssd ssd;
    public Ram ram;

    public Computer(com.ascennii.Ssd ssd, Ram ram) {
        this.ssd = ssd;
        this.ram = ram;
    }

    public Computer() {
        System.out.println("Computer constructor");
    }

    public com.ascennii.Ssd getSsd() {
        return ssd;
    }

    public Ram getRam() {
        return ram;
    }

//    public void load(){
//        System.out.println("Computer is load");
//    }

    public abstract void load();

    public void print() {
        System.out.println("Ssd: " + ssd.getValue() + "\n" + "Ram: " + ram.getValue());
    }
}
