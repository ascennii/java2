package com.ascennii.task;

public class TimeIntervalRunner {

    public static void main(String[] args) {
        TimeInterval timeInterval1 = new TimeInterval(60);
        TimeInterval timeInterval2 = new TimeInterval(3600);

        TimeInterval.sumInterval(timeInterval1, timeInterval2).printTimeInterval();

    }
}
