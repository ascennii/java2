package com.ascennii.task;

public class TimeInterval {
    private int hours;
    private int minutes;
    private int seconds;

    public TimeInterval(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public TimeInterval(int totalSeconds) {
        this.hours = totalSeconds / 3600;
        this.minutes = totalSeconds % 3600 / 60;
        this.seconds = totalSeconds % 3600 % 60;
    }

    public int totalSeconds() {
        return seconds + minutes * 60 + hours * 3600;
    }

    public void printTimeInterval() {
        System.out.println("TimeInterval: " + hours + "H " + minutes + "M " + seconds + "S ");
    }

    public static TimeInterval sumInterval(TimeInterval tm1, TimeInterval tm2){

        int seconds = tm1.totalSeconds() + tm2.totalSeconds();
        return new TimeInterval(seconds);
    }
}
